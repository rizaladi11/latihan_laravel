<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
   <form action="/welcome">
       @csrf
       <label>First name:</label><br><br>
        <input type="text" name="nama"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="namakedua"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="jk" value="1">Male <br>
        <input type="radio" name="jk" value="2">Female <br>
        <input type="radio" name="jk" value="3">Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="negara">
            <option value="1">Indonesian</option>
            <option value="2">English</option>
            <option value="3">Malaysian</option>
        </select> <br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa" value="1">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa" value="2">English <br>
        <input type="checkbox" name="bahasa" value="3">Arabic <br>
        <input type="checkbox" name="bahasa" value="4">Japanese <br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>      
        <input type="submit" value="Sign UP">
       
   </form>
</body>
</html>