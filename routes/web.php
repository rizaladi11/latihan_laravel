<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ROUTE UNTUK STATIC WEB 

Route::get('/','HomeController@dashboard');
Route::get('/register','AuthController@register');
Route::get('/welcome','AuthController@welcome');

// ROUTE UNTUK TABLE TEMPLATE

Route::get('/', function () {
    return view('layout.master');
});

Route::get('/table', function () {
    return view('halaman.table');
});

Route::get('/data-table', function () {
    return view('halaman.data-table');
});


// CRUD Cast

// Create
// form input data tambah cast
Route::get('/cast/create', 'castController@create');
// Menyimpan data ke database
Route::post('/cast', 'castController@store');

// Read
// Menampilkan semua data di table cast
Route::get('/cast', 'castController@index');
// Menampilkan detail cast berdasarkan id 
Route::get('/cast/{cast_id}','castController@show');

// Update
// form edit data cast
Route::get('/cast/{cast_id}/edit','castController@edit');
// untuk update data berdasarkan id di tabel cast
Route::put('/cast/{cast_id}','castController@update');

// Delete
Route::delete('/cast/{cast_id}','castController@destroy');

